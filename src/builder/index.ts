/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Forms,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    isBoolean,
    pgettext,
    slots,
    tripetto,
} from "@tripetto/builder";
import { PasswordCondition } from "./condition";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:password", "Password");
    },
})
export class Password extends NodeBlock {
    passwordSlot!: Slots.String;

    @definition
    @affects("#slots")
    exportable?: boolean;

    @slots
    defineSlot(): void {
        this.passwordSlot = this.slots.static({
            type: Slots.String,
            reference: "password",
            label: Password.label,
            exportable: this.exportable || false,
            exchange: ["required", "alias"],
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        this.editor.groups.options();
        this.editor.required(this.passwordSlot);
        this.editor.visibility();
        this.editor.alias(this.passwordSlot);

        const dataWarning = this.editor
            .form({
                controls: [
                    new Forms.Notification(
                        pgettext(
                            "block:password",
                            "By default the submitted passwords will not be stored in the dataset (assuming you just want to verify a password). If you do need the password in the dataset, please use the *exportability* feature."
                        ),
                        "info"
                    ).markdown(),
                ],
            })
            .visible(!isBoolean(this.exportable));

        this.editor
            .exportable(this, false)
            .onToggle((exportable) =>
                dataWarning.visible(!exportable.isActivated)
            );
    }

    @conditions
    defineCondition(): void {
        each(
            [
                {
                    invert: undefined,
                    label: pgettext("block:password", "Password matches"),
                },
                {
                    invert: true as const,
                    label: pgettext(
                        "block:password",
                        "Password does not match"
                    ),
                },
            ],
            (condition) => {
                this.conditions.template({
                    condition: PasswordCondition,
                    label: condition.label,
                    burst: true,
                    props: {
                        slot: this.passwordSlot,
                        invert: condition.invert,
                    },
                });
            }
        );
    }
}
