/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    AES,
    ConditionBlock,
    Forms,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "@tripetto/builder";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:password", "Password match");
    },
})
export class PasswordCondition extends ConditionBlock {
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    passphrase = "";

    @definition
    @affects("#name")
    invert: undefined | true;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot) {
            return `@${this.slot.id} ${this.invert ? "\u2260" : "="} ${
                this.passphrase ? "`•••`" : "\\_\\_"
            }`;
        }

        return this.type.label;
    }

    get title() {
        return this.node?.label;
    }

    @editor
    defineEditor(): void {
        if (this.id) {
            const setPassword = () => {
                if (passwordA.value || passwordB.value) {
                    if (passwordA.value === passwordB.value) {
                        this.passphrase = AES.encrypt(
                            this.id,
                            passwordA.value + this.id,
                            256,
                            btoa
                        );
                    } else {
                        this.passphrase = "";
                    }
                }

                validPassword.visible(this.passphrase ? true : false);
                invalidPassword.visible(this.passphrase ? false : true);
            };

            const validPassword = new Forms.Notification(
                pgettext("block:password", "Password is set!"),
                "success"
            ).visible(this.passphrase ? true : false);

            const invalidPassword = new Forms.Notification(
                pgettext("block:password", "Password not set!"),
                "warning"
            ).visible(this.passphrase ? false : true);

            const passwordA = new Forms.Text("password")
                .label(pgettext("block:password", "Password"))
                .autoFocus()
                .on(setPassword);

            const passwordB = new Forms.Text("password")
                .label(pgettext("block:password", "Confirm password"))
                .on(setPassword);

            this.editor.form({
                title: pgettext("block:password", "Set password"),
                controls: [
                    new Forms.Static(
                        pgettext(
                            "block:password",
                            "You can use this condition block to verify a password. The password you set here is _never_ stored in Tripetto. Instead we use *asymmetric cryptography* to verify passwords without knowing them. As a result, you cannot see the password you set here and the fields below might be empty if a password is set. If the message *Password is set* appears, you have set a valid password and you are good to go. To set another password, enter the new password in both fields."
                        )
                    ).markdown(),
                    new Forms.Spacer(),
                    validPassword,
                    invalidPassword,
                    passwordA,
                    passwordB,
                    new Forms.Dropdown<undefined | true>(
                        [
                            {
                                label: pgettext(
                                    "block:password",
                                    "Verify if password is right"
                                ),
                                value: undefined,
                            },
                            {
                                label: pgettext(
                                    "block:password",
                                    "Verify if password is wrong"
                                ),
                                value: true,
                            },
                        ],
                        Forms.Dropdown.bind(this, "invert", undefined)
                    )
                        .label(pgettext("block:password", "Mode"))
                        .width("full"),
                ],
            });
        }
    }
}
