/** Dependencies */
import { NodeBlock, Slots, assert } from "@tripetto/runner";
import "./condition";

export abstract class Password extends NodeBlock {
    /** Contains the passowrd slot with the value. */
    readonly passwordSlot = assert(
        this.valueOf<string, Slots.String>("password")
    );

    /** Contains if the block is required. */
    readonly required = this.passwordSlot.slot.required || false;
}
