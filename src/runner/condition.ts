/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    AES,
    ConditionBlock,
    condition,
    isBoolean,
    tripetto,
} from "@tripetto/runner";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
})
export class PasswordCondition extends ConditionBlock<{
    readonly passphrase: string;
    readonly invert?: true;
}> {
    private cache: {
        [password: string]: boolean;
    } = {};

    @condition
    isPassword(): boolean {
        const passwordSlot = this.valueOf<string>();

        if (this.props.passphrase && passwordSlot && passwordSlot.value) {
            return (
                (isBoolean(this.cache[passwordSlot.value])
                    ? this.cache[passwordSlot.value]
                    : (this.cache[passwordSlot.value] =
                          AES.decrypt(
                              this.props.passphrase,
                              passwordSlot.value + this.condition.id,
                              256,
                              atob
                          ) === this.condition.id)) ===
                (this.props.invert ? false : true)
            );
        }

        return false;
    }
}
